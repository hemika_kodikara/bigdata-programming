# bigdata-programming

## Trimming Data
`sed -n 1,200p yellow_tripdata_2016-03.csv > ../trimmed-data/yellow_tripdata_2016-03.csv`

## Hadoop

### Total Money Earned
```bash
mvn clean install -f /Users/hemikakodikara/personal/big-data-programming/hadoop-tasks/pom.xml \
&& \
sh /Users/hemikakodikara/Documents/week4-resources/hadoop-2.7.3/bin/hadoop \
jar /Users/hemikakodikara/personal/big-data-programming/hadoop-tasks/target/big-data-hadoop-work-1.0-SNAPSHOT.jar iit.ac.hemikak.tasks.TotalMoneyEarned \
/Users/hemikakodikara/personal/big-data-programming/cw-data/ \
/Users/hemikakodikara/personal/big-data-programming/hadoop-tasks/target/out/
```

### Rides Per Day
```bash
mvn clean install -f /Users/hemikakodikara/personal/big-data-programming/hadoop-tasks/pom.xml \
&& \
sh /Users/hemikakodikara/Documents/week4-resources/hadoop-2.7.3/bin/hadoop \
jar /Users/hemikakodikara/personal/big-data-programming/hadoop-tasks/target/big-data-hadoop-work-1.0-SNAPSHOT.jar iit.ac.hemikak.tasks.RidesOnEachDay \
/Users/hemikakodikara/personal/big-data-programming/cw-data/ \
/Users/hemikakodikara/personal/big-data-programming/hadoop-tasks/target/out/
```

### Average Number of Passengers Per Hour for All Days
```bash
mvn clean install -f /Users/hemikakodikara/personal/big-data-programming/hadoop-tasks/pom.xml \
&& \
sh /Users/hemikakodikara/Documents/week4-resources/hadoop-2.7.3/bin/hadoop \
jar /Users/hemikakodikara/personal/big-data-programming/hadoop-tasks/target/big-data-hadoop-work-1.0-SNAPSHOT.jar iit.ac.hemikak.tasks.AvgNoPassengersPerHourAllDays \
/Users/hemikakodikara/personal/big-data-programming/cw-data/ \
/Users/hemikakodikara/personal/big-data-programming/hadoop-tasks/target/out/
```

## Hive on Hadoop
```bash
sudo mkdir -p /user/hive/warehouse
sudo chmod g+w /user/hive/warehouse/
$HADOOP_HOME/bin/hadoop fs -chmod g+w /user/hive/warehouse
$HIVE_HOME/bin/schematool -dbType derby -initSchema
```

### Command line
`cd /Users/hemikakodikara/personal/big-data-programming/hive-meta`  
`$HIVE_HOME/bin/schematool -dbType derby -initSchema`  
`sh $HIVE_HOME/bin/hive`  

### Creating Taxi Table
Load data to table.
```sql
DROP TABLE IF EXISTS taxi_data;
CREATE EXTERNAL TABLE IF NOT EXISTS taxi_data (
    VendorID INT,
    tpep_pickup_datetime TIMESTAMP,
    tpep_dropoff_datetime TIMESTAMP,
    passenger_count INT,
    trip_distance DOUBLE,
    pickup_longitude DOUBLE,
    pickup_latitude DOUBLE,
    RatecodeID INT,
    store_and_fwd_flag VARCHAR(255),
    dropoff_longitude DOUBLE,
    dropoff_latitude DOUBLE,
    payment_type INT,
    fare_amount DOUBLE,
    extra DOUBLE,
    mta_tax DOUBLE,
    tip_amount DOUBLE,
    tolls_amount DOUBLE,
    improvement_surcharge DOUBLE,
    total_amount DOUBLE
)
COMMENT 'Taxi Data'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
STORED AS TEXTFILE
LOCATION '/Users/hemikakodikara/personal/big-data-programming/cw-data/'
TBLPROPERTIES (
    'skip.header.line.count' = '1');
```

### Calculating Average Travel Time per each Pickup Hour
Group by Hour
```sql
DROP TABLE IF EXISTS travel_time_diff_per_hour;
CREATE TEMPORARY TABLE travel_time_diff_per_hour AS
SELECT substr(tpep_pickup_datetime, 11, 3) AS hour,
(SUM(unix_timestamp(tpep_dropoff_datetime) - unix_timestamp(tpep_pickup_datetime)) * 1000) AS diff
FROM taxi_data
GROUP BY substr(tpep_pickup_datetime, 11, 3);
```

Group by Count
```sql
DROP TABLE IF EXISTS rides_per_hour;
CREATE TEMPORARY TABLE rides_per_hour AS
SELECT substr(tpep_pickup_datetime, 11, 3) AS hour,
COUNT(*) AS total_rides
FROM taxi_data
GROUP BY substr(tpep_pickup_datetime, 11, 3);
```

Calculate by Average Time Travel
```sql
SELECT ttd.hour, (ttd.diff / rh.total_rides)
FROM travel_time_diff_per_hour AS ttd JOIN rides_per_hour AS rh
ON (ttd.hour = rh.hour)
ORDER BY ttd.hour;
```

### Top 10 pickup cells & Top 10 drop-off cells
(73.892880 - 74.019342) / 100 = 0.126462 / 100 = 0.00126462 <-- Longitude Unit  
(40.845229 - 40.698187) / 100 = 0.147042 / 100 = 0.00147042 <-- Latitude Unit  

```sql
DROP TABLE IF EXISTS location_cells;

CREATE TEMPORARY TABLE location_cells AS
SELECT (0.00126462 * floor(abs(pickup_longitude) / 0.00126462)) as pickup_long_start,
(0.00126462 + (0.00126462 * floor(abs(pickup_longitude) / 0.00126462))) as pickup_long_end,
(0.00147042 * floor(abs(pickup_latitude) / 0.00147042)) as pickup_lat_start,
(0.00147042 + (0.00147042 * floor(abs(pickup_latitude) / 0.00147042))) as pickup_lat_end,

(0.00126462 * floor(abs(dropoff_longitude) / 0.00126462)) AS dropoff_long_start,
(0.00126462 + (0.00126462 * floor(abs(dropoff_longitude) / 0.00126462))) AS dropoff_long_end,
(0.00147042 * floor(abs(dropoff_latitude) / 0.00147042)) AS dropoff_lat_start,
(0.00147042 + (0.00147042 * floor(abs(dropoff_latitude) / 0.00147042))) AS dropoff_lat_end
FROM taxi_data;
```

Calculation Top 10 Pickup Cells.
```sql
SELECT pickup_long_start, pickup_long_end, pickup_lat_start, pickup_lat_end, COUNT(*) as cnt
FROM location_cells
GROUP BY pickup_long_start, pickup_long_end, pickup_lat_start, pickup_lat_end
ORDER BY cnt DESC
LIMIT 10;
```

Calculation Top 10 Dropoff Cells.
```sql
SELECT dropoff_long_start, dropoff_long_end, dropoff_lat_start, dropoff_lat_end, COUNT(*) as cnt
FROM location_cells
GROUP BY dropoff_long_start, dropoff_long_end, dropoff_lat_start, dropoff_lat_end
ORDER BY cnt DESC
LIMIT 10;
```

### Percentage of Different payment-type
source - http://www.nyc.gov/html/tlc/downloads/pdf/data_dictionary_trip_records_yellow.pdf
```sql
SELECT
      (SUM(IF(payment_type='1', 1, 0)) / count(1)) * 100 as Credit_Card,
      (SUM(IF(payment_type='2', 1, 0)) / count(1)) * 100 as Cash,
      (SUM(IF(payment_type='3', 1, 0)) / count(1)) * 100 as No_Charge,
      (SUM(IF(payment_type='4', 1, 0)) / count(1)) * 100 as Dispute,
      (SUM(IF(payment_type='5', 1, 0)) / count(1)) * 100 as Unknown,
      (SUM(IF(payment_type='6', 1, 0)) / count(1)) * 100 as Voided_Trip
FROM
    taxi_data;
```

## Website
### Docker image  
Building and Pushing  
`docker build -t hemikak/taxi-data-analysis .`  
`docker login`  
`docker push hemikak/taxi-data-analysis`  

Pulling and Starting Up.  
`docker pull hemikak/taxi-data-analysis`  
`docker images`  
`docker service create --name taxi-data-analysis-site -p 3333:5000 --with-registry-auth --detach=false hemikak/taxi-data-analysis`  
[SEE ONLINE](http://shopaholi-external-270ohjt33tdj-449932389.us-east-1.elb.amazonaws.com:3333/)
