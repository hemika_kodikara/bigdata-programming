package iit.ac.hemikak

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.functions.{udf, lit, count, desc}
import math._

/**
  * The top ten pickup and dropoff combinations.
  */
object TopTenPickupAndDropOff {
    val LONGITUDE_UNIT = 0.00126462
    val LATITUDE_UNIT = 0.00147042
    def main(args: Array[String]): Unit = {
        val conf = new SparkConf().setAppName("Top Ten Pickups and Drop Off Location Combinations").setMaster("local")
        val sc = new SparkContext(conf)

        val sqlContext = new SQLContext(sc)
        var df = sqlContext.read
            .format("com.databricks.spark.csv")
            .option("header", "true") // Use first line of all files as header
            .option("inferSchema", "true") // Automatically infer data types
            .load("/Users/hemikakodikara/personal/big-data-programming/cw-data/*")

        val getPickupStart = udf((location: Double, unit: Double) => {
            unit * floor(abs(location) / unit)
        })
        val getPickupEnd = udf((pickup_longitude: Double, unit: Double) => {
            unit + (unit * floor(abs(pickup_longitude) / unit))
        })
        df = df.withColumn("pickup_long_start", getPickupStart(df("pickup_longitude"), lit(LONGITUDE_UNIT)))
            .withColumn("pickup_long_end", getPickupEnd(df("pickup_longitude"), lit(LONGITUDE_UNIT)))
            .withColumn("pickup_lat_start", getPickupStart(df("pickup_latitude"), lit(LATITUDE_UNIT)))
            .withColumn("pickup_lat_end", getPickupEnd(df("pickup_latitude"), lit(LATITUDE_UNIT)))

            .withColumn("dropoff_long_start", getPickupStart(df("dropoff_longitude"), lit(LONGITUDE_UNIT)))
            .withColumn("dropoff_long_end", getPickupEnd(df("dropoff_longitude"), lit(LONGITUDE_UNIT)))
            .withColumn("dropoff_lat_start", getPickupStart(df("dropoff_latitude"), lit(LATITUDE_UNIT)))
            .withColumn("dropoff_lat_end", getPickupEnd(df("dropoff_latitude"), lit(LATITUDE_UNIT)))

        df.groupBy("pickup_long_start", "pickup_long_end", "pickup_lat_start", "pickup_lat_end",
                    "dropoff_long_start", "dropoff_long_end", "dropoff_lat_start", "dropoff_lat_end")
            .agg(count("*")
                .alias("cnt"))
            .orderBy(desc("cnt"))
            .limit(10)
            .show(true)
    }
}
