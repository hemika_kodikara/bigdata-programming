package iit.ac.hemikak

import java.io.File

import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.sql.functions.{count, udf}

import math._
import org.apache.commons.io.FileUtils

/**
  * Decides whether a ride is direct or diverted.
  */
object DirectVDiverted {
    val R = 6372.8 //radius in km
    def main(args: Array[String]): Unit = {
        val conf = new SparkConf().setAppName("Direct vs Diverted").setMaster("local")
        val sc = new SparkContext(conf)

        val sqlContext = new SQLContext(sc)
        var df = sqlContext.read
            .format("com.databricks.spark.csv")
            .option("header", "true") // Use first line of all files as header
            .option("inferSchema", "true") // Automatically infer data types
            .load("/Users/hemikakodikara/personal/big-data-programming/cw-data/*")

        // Deleting output folder
        FileUtils.deleteDirectory(new File("/Users/hemikakodikara/personal/big-data-programming/spark-tasks/target/out"))

        // Sampling 10000
        df = getRandom(df, 10000)

        val getDistanceVal = udf((tripDistance: Double, lat1: Double, lon1: Double, lat2: Double, lon2: Double) => {
            val displacement = getDistance(lat1, lon1, lat2, lon2)
            if (tripDistance <= displacement) {
                "Invalid"
            } else if (tripDistance <= (displacement * 1.2)) {
                "Direct"
            } else {
                "Diverted"
            }
        })

        df = df.withColumn("Direct_or_Diverted", getDistanceVal(df("trip_distance"),
            df("pickup_latitude"), df("pickup_longitude"), df("dropoff_latitude"), df("dropoff_longitude")))

        df.groupBy("Direct_or_Diverted")
            .agg(count("Direct_or_Diverted"))
            .show(false)

        df.write
            .format("com.databricks.spark.csv")
            .save("/Users/hemikakodikara/personal/big-data-programming/spark-tasks/target/out")
    }

    // https://github.com/acmeism/RosettaCodeData/blob/master/Task/Haversine-formula/Scala/haversine-formula.scala
    def getDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double = {
        val dLat = (lat2 - lat1).toRadians
        val dLon = (lon2 - lon1).toRadians

        val a = pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(lat1.toRadians) * cos(lat2.toRadians)
        val c = 2 * asin(sqrt(a))
        R * c * 0.62137
    }

    // https://stackoverflow.com/questions/39344769/spark-dataframe-select-n-random-rows
    def getRandom(dataFrame: DataFrame, n: Int): DataFrame = {
        val count = dataFrame.count()
        val howManyTake = if (count > n) n else count
        dataFrame.sample(withReplacement = false, 1D * howManyTake / count).limit(n)
    }
}
