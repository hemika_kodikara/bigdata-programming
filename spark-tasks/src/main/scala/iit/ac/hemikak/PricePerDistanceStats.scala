package iit.ac.hemikak

import java.io.File

import org.apache.commons.io.FileUtils
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.functions.{avg, desc, max, min, udf}

/**
  * Gets the Stats for Price Per Distance
  */
object PricePerDistanceStats {
    def main(args: Array[String]): Unit = {
        val conf = new SparkConf().setAppName("Get Stats").setMaster("local")
        val sc = new SparkContext(conf)

        val sqlContext = new SQLContext(sc)
        var df = sqlContext.read
            .format("com.databricks.spark.csv")
            .option("header", "true") // Use first line of all files as header
            .option("inferSchema", "true") // Automatically infer data types
            .load("/Users/hemikakodikara/personal/big-data-programming/cw-data/*")

        // Deleting output folder
        FileUtils.deleteDirectory(new File("/Users/hemikakodikara/personal/big-data-programming/spark-tasks/target/out"))

        val extractHour: (String => String) = (arg: String) => {
            arg.split(" ")(1).split(":")(0)
        }
        val extractHourUDF = udf(extractHour)
        df = df.withColumn("Hour", extractHourUDF(df("tpep_pickup_datetime")))

        def calculatePricePerDistance = udf((distance: Float, totalAmount: Float) => {
            if (totalAmount != 0 && distance != 0) {
                totalAmount / distance
            } else {
                0
            }
        })

        df = df.withColumn("PricePerDistance", calculatePricePerDistance(df("trip_distance"), df("total_amount")))

        println("Calculated Price Per Distance")
        df.select("tpep_pickup_datetime", "Hour", "PricePerDistance")
            .where(df("Hour") === 12)
            .orderBy(desc("PricePerDistance"))
            .show(10000, truncate = false)

        println("Stats")
        df = df.groupBy("Hour")
            .agg(max("PricePerDistance").alias("Max_PricePerDistance"),
                min("PricePerDistance").alias("Min_PricePerDistance"),
                avg("PricePerDistance").alias("Average_PricePerDistance"))

        df.write
            .format("com.databricks.spark.csv")
            .save("/Users/hemikakodikara/personal/big-data-programming/spark-tasks/target/out")
    }
}
