import React from 'react';
import Paper from 'material-ui/Paper';
import { XYPlot, VerticalBarSeries, VerticalGridLines, HorizontalGridLines, XAxis, YAxis, Hint } from 'react-vis';
import cvs from 'csvtojson';
import csvData from '../data/price-per-distance-data';


const style = {
    margin: 20,
    padding: 15,
    display: 'inline-block',
};

export class PricePerDistanceStats extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
            ]
        }

        this.saveHintValue = this.saveHintValue.bind(this);
        this.removeHintValue = this.removeHintValue.bind(this);
    }
    componentDidMount() {
            cvs({
                noheader: true,
                headers: ["x", "Max_PricePerDistance", "Min_PricePerDistance", "Average_PricePerDistance"],
            })
                .fromString(csvData)
                .on('json', (jsonObjct) => {
                        jsonObjct.y = 
                        jsonObjct.color = "#4DB6AC";
                        this.setState({
                            data: [...this.state.data,
                                {
                                    x: jsonObjct.x,
                                    y: jsonObjct.Max_PricePerDistance,
                                    color: "#4DB6AC",
                                },
                                {
                                    x: jsonObjct.x,
                                    y: jsonObjct.Average_PricePerDistance,
                                    color: "#AED581",
                                },
                                {
                                    x: jsonObjct.x,
                                    y: jsonObjct.Min_PricePerDistance,
                                    color: "#DCE775",
                                }],
                        });
                });
    }

    saveHintValue(value) {
        this.setState({ value });
    }

    removeHintValue() {
        this.setState({ value: null });
    }

    getDataForLegend() {
        const averageRows = this.state.data.filter((row) => {
            return row.color === "#AED581";
        })
        return averageRows.map((row) => {
            return {
                title: `${row.y} at ${row.x} Hours`,
            }
        })
    }

    render() {
        const { value } = this.state;
        return (<Paper style={style} zDepth={1}>
            <h3><strong>Price per Distance Statistics(Max, Average, Min) Chart</strong></h3>
            <XYPlot
                height={300}
                width={1100}
                xType="ordinal"
                yType="linear">
                <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis />
                <YAxis />
                <VerticalBarSeries
                    data={this.state.data}
                    onValueMouseOver={this.saveHintValue}
                    onValueMouseOut={this.removeHintValue}
                    colorType="literal"
                />
                {value ?
                    <Hint value={value}>
                        <div style={{
                            background: '#37474F',
                            padding: 10,
                        }}>
                            {value.y} Price per Distance
                        </div>
                    </Hint>
                    :
                    null
                }
            </XYPlot>
        </Paper>)
    }
}

export default PricePerDistanceStats;


