import React from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import cvs from 'csvtojson';
import csvData from '../data/top-ten-pick-up-drop-off-cells';

const style = {
    width: 1400,
    margin: 20,
    padding: 15,
    display: 'inline-block',
};

export class TopTenPickUpAndDropOffCells extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: [] }
    }
    componentDidMount() {
        cvs({
            noheader: true,
            headers: ["pickup_long_start", "pickup_long_end", "pickup_lat_start", "pickup_lat_end", 
                        "dropoff_long_start", "dropoff_long_end", "dropoff_lat_start", "dropoff_lat_end", "count"],
        })
            .fromString(csvData)
            .on('json', (jsonObjct) => {
                this.setState({
                    data: [...this.state.data, jsonObjct]
                })
            })
    }
    renderRows() {
        return this.state.data.map((row) => {
            return (<TableRow key={row.count}>
                <TableRowColumn key={`-${row.pickup_long_start} : -${row.pickup_long_end}`}>
                    {`-${row.pickup_long_start} : -${row.pickup_long_end}`}
                </TableRowColumn>
                <TableRowColumn key={`${row.pickup_lat_start} : ${row.pickup_lat_end}`}>
                    {`${row.pickup_lat_start} : ${row.pickup_lat_end}`}
                </TableRowColumn>
                <TableRowColumn key={`-${row.dropoff_long_start} : -${row.dropoff_long_end}`}>
                    {`-${row.dropoff_long_start} : -${row.dropoff_long_end}`}
                </TableRowColumn>
                <TableRowColumn key={`${row.dropoff_lat_start} : ${row.dropoff_lat_end}`}>
                    {`${row.dropoff_lat_start} : ${row.dropoff_lat_end}`}
                </TableRowColumn>
                <TableRowColumn>{row.count}</TableRowColumn>
            </TableRow>);
        })
    }

    render() { 
        const rows = this.renderRows();
        return ( 
            <Paper style={style} zDepth={1}>
                <h3><strong>Top Ten Pickup and Drop Off Cell Combinations</strong></h3>
                <Table>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                        <TableRow>
                            <TableHeaderColumn>Pickup Longitude Range</TableHeaderColumn>
                            <TableHeaderColumn>Pickup Latitude Range</TableHeaderColumn>
                            <TableHeaderColumn>Drop Off Longitude Range</TableHeaderColumn>
                            <TableHeaderColumn>Drop Off Latitude Range</TableHeaderColumn>
                            <TableHeaderColumn>Pickup Count</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {rows}
                    </TableBody>
                </Table>
            </Paper>
         )
    }
}
 
export default TopTenPickUpAndDropOffCells;