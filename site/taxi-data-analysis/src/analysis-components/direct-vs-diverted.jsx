import React from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import cvs from 'csvtojson';
import csvData from '../data/direct-vs-diverted-partial.js';

const style = {
    margin: 20,
    padding: 15,
    display: 'inline-block',
};

export class DirectVsDiverted extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: [] }
    }
    componentDidMount() {
        cvs({
            noheader: true,
            headers: ["VendorID", "tpep_pickup_datetime", "tpep_dropoff_datetime", "passenger_count", "trip_distance",
                "pickup_longitude", "pickup_latitude", "RatecodeID", "store_and_fwd_flag", "dropoff_longitude",
                "dropoff_latitude", "payment_type", "fare_amount", "extra", "mta_tax", "tip_amount", "tolls_amount",
                "improvement_surcharge", "total_amount", "direct_vs_diverted"],
        })
            .fromString(csvData)
            .on('json', (jsonObjct) => {
                this.setState({
                    data: [...this.state.data, jsonObjct]
                })
            })
    }
    renderRows() {
        return this.state.data.map((row) => {
            return (<TableRow key={row.tpep_pickup_datetime}>
                <TableRowColumn key={`${row.tpep_pickup_datetime}-${row.trip_distance}`}>
                    {row.trip_distance}
                </TableRowColumn>
                <TableRowColumn key={`${row.pickup_longitude}-${row.pickup_latitude}`}>
                    {`${row.pickup_longitude} : ${row.pickup_latitude}`}
                </TableRowColumn>
                <TableRowColumn key={`${row.dropoff_longitude}-${row.dropoff_latitude}`}>
                    {`${row.dropoff_longitude} : ${row.dropoff_latitude}`}
                </TableRowColumn>
                <TableRowColumn key={`${row.tpep_pickup_datetime}-${row.direct_vs_diverted}`}>
                    {row.direct_vs_diverted}
                </TableRowColumn>
            </TableRow>);
        })
    }

    render() {
        const rows = this.renderRows();
        return (
            <Paper style={style} zDepth={1}>
                <h3><strong>Direct vs. Diverted vs. Invalid Rides(100 rows)</strong></h3>
                <Table>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                        <TableRow>
                            <TableHeaderColumn>Actual Distance(Miles)</TableHeaderColumn>
                            <TableHeaderColumn>Pickup Location (Logitude:Latitude)</TableHeaderColumn>
                            <TableHeaderColumn>Drop Off Location (Logitude:Latitude)</TableHeaderColumn>
                            <TableHeaderColumn>Direct vs. Diverted</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {rows}
                    </TableBody>
                </Table>
            </Paper>
        )
    }
}

export default DirectVsDiverted;