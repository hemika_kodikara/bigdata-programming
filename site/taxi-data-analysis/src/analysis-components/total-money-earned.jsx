import React from 'react';
import Paper from 'material-ui/Paper';

const style = {
    height: 135,
    width: 240,
    margin: 20,
    padding: 15,
    display: 'inline-block',
};

export default (<Paper style={style} zDepth={1}>
    <h3><strong>Total Money Earned</strong></h3>
    <h2>$ 544,110,881.43</h2>
</Paper>
);
