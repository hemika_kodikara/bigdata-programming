import React from 'react';
import Paper from 'material-ui/Paper';
import { XYPlot, ArcSeries, DiscreteColorLegend } from 'react-vis';
import { indigo400, cyan400, purple400, teal400, yellow400, lime400 } from 'material-ui/styles/colors';
import csvData from '../data/payment-percentages';

const style = {
    height: 570,
    margin: 20,
    padding: 15,
    display: 'inline-block',
};

const COLOR_ARRAY = [indigo400, cyan400, purple400, teal400, yellow400, lime400];

export class AveragePassengersPerHour extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.getDataForPie()
        }

        this.saveHintValue = this.saveHintValue.bind(this);
        this.removeHintValue = this.removeHintValue.bind(this);
    }

    getDataForPie() {
        const percentages = csvData.split(',')
        let angleStart = 0;
        let angelEnd;
        return percentages.map((percentage, index) => {
            if (index === percentages.length - 1) {
                angelEnd = 360;
            } else {
                angelEnd = angleStart + (3.6 * parseFloat(percentage));
            }
            var arcData = {
                angle0: angelEnd * 0.0174533,
                angle: angleStart * 0.0174533,
                radius0: 100,
                radius: 100,
                color: COLOR_ARRAY[index]
            }

            angleStart = angelEnd;
            return arcData;
        });
    }
    
    getDataForLegend() {
        const percentages = csvData.split(',')
        return [
            {
                title: `Credit Card - ${percentages[0]}%`,
                color: COLOR_ARRAY[0]
            },
            {
                title: `Cash - ${percentages[1]}%`,
                color: COLOR_ARRAY[1]
            },
            {
                title: `No Charge - ${percentages[2]}%`,
                color: COLOR_ARRAY[2]
            },
            {
                title: `Dispute - ${percentages[3]}%`,
                color: COLOR_ARRAY[3]
            },
            {
                title: `Unknown - ${percentages[4]}%`,
                color: COLOR_ARRAY[4]
            },
            {
                title: `Voided Trip - ${percentages[5]}%`,
                color: COLOR_ARRAY[5]
            },
        ];
    }

    saveHintValue(value) {
        this.setState({ value });
    }

    removeHintValue() {
        this.setState({ value: null });
    }

    render() {
        return (<Paper style={style} zDepth={1}>
            <h3><strong>Payment Percentages</strong></h3>
            <XYPlot
                xDomain={[-5, 5]}
                yDomain={[-5, 5]}
                width={300}
                height={300}>
                <ArcSeries
                    animation
                    radiusType={'literal'}
                    colorType="literal"
                    data={this.state.data}
                    onValueMouseOver={this.saveHintValue}
                    onValueMouseOut={this.removeHintValue} />
                <DiscreteColorLegend items={this.getDataForLegend()}/>
            </XYPlot>
        </Paper>)
    }
}

export default AveragePassengersPerHour;


