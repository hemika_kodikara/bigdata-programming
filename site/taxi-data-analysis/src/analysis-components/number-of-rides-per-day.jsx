import React from 'react';
import Paper from 'material-ui/Paper';
import { XYPlot, VerticalBarSeries, VerticalGridLines, HorizontalGridLines, XAxis, YAxis, Hint } from 'react-vis';
import cvs from 'csvtojson';
import csvData from '../data/number-of-rides-per-day';

const style = {
    margin: 20,
    padding: 15,
    display: 'inline-block',
};

export class NumberOfRidesPerDay extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
            ]
        }

        this.saveHintValue = this.saveHintValue.bind(this);
        this.removeHintValue = this.removeHintValue.bind(this);
    }
    componentDidMount() {
        cvs({
            noheader: true,
            headers: ["x", "y"],
            colParser: {
                "x": "string",
                "y": "number"
            }
        })
            .fromString(csvData)
            .on('json', (jsonObjct) => {
                this.setState({
                    data: [...this.state.data, jsonObjct]
                })
            })
    }

    saveHintValue(value) {
        this.setState({ value });
    }
    
    removeHintValue() {
        this.setState({ value: null });
    }

    render() { 
        const { value } = this.state;
        return (<Paper style={style} zDepth={1}>
            <h3><strong>Number of Rides Per Day</strong></h3>
            <XYPlot
                height={300}
                width={1400}
                xType="ordinal"
                yType="linear">
                    <VerticalGridLines />
                    <HorizontalGridLines />
                    <XAxis />
                    <YAxis />
                    <VerticalBarSeries
                        data={this.state.data}
                        onValueMouseOver={this.saveHintValue}
                        onValueMouseOut={this.removeHintValue}
                    />
                    {value ?
                        <Hint value={value}>
                            <div style={{
                                background: '#37474F',
                                padding: 10,
                            }}>
                                {value.y} Rides on {value.x}
                            </div>
                        </Hint>
                        :
                        null
                    }
            </XYPlot>
        </Paper>)
    }
}
 
export default NumberOfRidesPerDay;


