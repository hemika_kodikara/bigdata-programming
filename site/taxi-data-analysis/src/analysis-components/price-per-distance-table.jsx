import React from 'react';
import {
    Table,
    TableBody,
    TableHeader,
    TableHeaderColumn,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import Paper from 'material-ui/Paper';
import cvs from 'csvtojson';
import csvData from '../data/price-per-distance-data';

const style = {
    width: 750,
    margin: 20,
    padding: 15,
    display: 'inline-block',
};

export class PaymentTypeTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = { data: [] }
    }
    componentDidMount() {
        cvs({
            noheader: true,
            headers: ["hour", "max", "min", "average"],
        })
            .fromString(csvData)
            .on('json', (jsonObjct) => {
                this.setState({
                    data: [...this.state.data, jsonObjct]
                })
            })
    }
    renderRows() {
        return this.state.data.map((row) => {
            return (<TableRow key={row.hour + "-row"}>
                <TableRowColumn key={row.hour}>
                    {row.hour}
                </TableRowColumn>
                <TableRowColumn key={row.max}>
                    {row.max}
                </TableRowColumn>
                <TableRowColumn key={row.min}>
                    {row.min}
                </TableRowColumn>
                <TableRowColumn key={row.average}>
                    {row.average}
                </TableRowColumn>
            </TableRow>);
        })
    }

    render() {
        const rows = this.renderRows();
        return (
            <Paper style={style} zDepth={1}>
                <h3><strong>Price per Distance Statistics(Max, Average, Min)</strong></h3>
                <Table>
                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                        <TableRow>
                            <TableHeaderColumn>Hour</TableHeaderColumn>
                            <TableHeaderColumn>Maxiumum</TableHeaderColumn>
                            <TableHeaderColumn>Minimum</TableHeaderColumn>
                            <TableHeaderColumn>Average</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody displayRowCheckbox={false}>
                        {rows}
                    </TableBody>
                </Table>
            </Paper>
        )
    }
}

export default PaymentTypeTable;