import React from 'react';
import Paper from 'material-ui/Paper';
import { XYPlot, VerticalBarSeries, VerticalGridLines, HorizontalGridLines, XAxis, YAxis, Hint } from 'react-vis';
import cvs from 'csvtojson';
import csvData from '../data/average-travel-time-per-hour';

const style = {
    height: 400,
    margin: 20,
    padding: 15,
    display: 'inline-block',
};

export class AverageTavelTimePerHour extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
            ]
        }

        this.saveHintValue = this.saveHintValue.bind(this);
        this.removeHintValue = this.removeHintValue.bind(this);
        this.convertToReadable = this.convertToReadable.bind(this);
    }
    componentDidMount() {
        cvs({
            noheader: true,
            headers: ["x", "y"],
            colParser: {
                "x": "string",
                "y": "number",
            }
        })
            .fromString(csvData)
            .on('json', (jsonObjct) => {
                this.setState({
                    data: [...this.state.data, jsonObjct]
                })
            })
    }

    saveHintValue(value) {
        this.setState({ value });
    }

    removeHintValue() {
        this.setState({ value: null });
    }

    convertToReadable(ms) {
        let d, h, m, s;
        s = Math.floor(ms / 1000);
        m = Math.floor(s / 60);
        s = s % 60;
        h = Math.floor(m / 60);
        m = m % 60;
        d = Math.floor(h / 24);
        h = h % 24;
        return { d: d, h: h, m: m, s: s };
    }

    render() {
        const { value } = this.state;
        const timeInfo = value ? this.convertToReadable(value.y) : undefined;
        return (<Paper style={style} zDepth={1}>
            <h3><strong>Average Travel Hour per each Hour</strong></h3>
            <XYPlot
                height={300}
                width={600}
                xType="ordinal"
                yType="linear">
                <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis />
                <YAxis />
                <VerticalBarSeries
                    data={this.state.data}
                    onValueMouseOver={this.saveHintValue}
                    onValueMouseOut={this.removeHintValue}
                    color='#7E57C2'
                />
                {value ?
                    <Hint value={value}>
                        <div style={{
                            background: '#37474F',
                            padding: 10,
                        }}>
                            {timeInfo.d === 0 ? "" : timeInfo.d + " Days, "}
                            {timeInfo.h === 0 ? "" : timeInfo.h + " Hours, "}
                            {timeInfo.m === 0 ? "" : timeInfo.m + " Minutes, "}
                            {timeInfo.s === 0 ? "" : timeInfo.s + " Seconds "}
                            at {value.x} Hour
                        </div>
                    </Hint>
                    :
                    null
                }
            </XYPlot>
        </Paper>)
    }
}

export default AverageTavelTimePerHour;


