import React from 'react';
import Paper from 'material-ui/Paper';
import { XYPlot, ArcSeries, DiscreteColorLegend } from 'react-vis';
import { blue400, teal400, yellow400 } from 'material-ui/styles/colors';
import csvData from '../data/direct-vs-diverted-percentages';
import cvs from 'csvtojson';

const style = {
    height: 500,
    margin: 20,
    padding: 15,
    display: 'inline-block',
};

const COLOR_ARRAY = [blue400, teal400, yellow400];

export class DirectVsDivertedPie extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }

        this.saveHintValue = this.saveHintValue.bind(this);
        this.removeHintValue = this.removeHintValue.bind(this);
    }

    componentDidMount() {
        cvs({
            noheader: true,
            headers: ["type", "amount"]
        })
            .fromString(csvData)
            .on('json', (jsonObjct) => {
                jsonObjct.percentage = jsonObjct.amount / 100
                this.setState({
                    data: [...this.state.data, jsonObjct]
                })
            })
    }

    getDataForPie() {
        let angleStart = 0;
        let angelEnd;
        return this.state.data.map((row, index) => {
            if (index === 2) {
                angelEnd = 360;
            } else {
                angelEnd = angleStart + (3.6 * parseFloat(row.percentage));
            }
            var arcData = {
                angle0: angelEnd * 0.0174533,
                angle: angleStart * 0.0174533,
                radius0: 100,
                radius: 100,
                color: COLOR_ARRAY[index]
            }

            angleStart = angelEnd;
            return arcData;
        });
    }

    getDataForLegend() {
        return this.state.data.map((row, index) => {
            return {
                title: `${row.type} - ${row.amount}`,
                color: COLOR_ARRAY[index]
            }
        });
    }

    saveHintValue(value) {
        this.setState({ value });
    }

    removeHintValue() {
        this.setState({ value: null });
    }

    render() {
        return (<Paper style={style} zDepth={1}>
            <h3><strong>Direct vs. Diverted vs. Invalid</strong></h3>
            <XYPlot
                xDomain={[-5, 5]}
                yDomain={[-5, 5]}
                width={300}
                height={300}>
                <ArcSeries
                    animation
                    radiusType={'literal'}
                    colorType="literal"
                    data={this.getDataForPie()}
                    onValueMouseOver={this.saveHintValue}
                    onValueMouseOut={this.removeHintValue} />
                <DiscreteColorLegend items={this.getDataForLegend()} />
            </XYPlot>
        </Paper>)
    }
}

export default DirectVsDivertedPie;


