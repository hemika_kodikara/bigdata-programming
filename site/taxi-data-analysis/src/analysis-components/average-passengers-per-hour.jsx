import React from 'react';
import Paper from 'material-ui/Paper';
import { XYPlot, VerticalBarSeries, VerticalGridLines, HorizontalGridLines, XAxis, YAxis, Hint } from 'react-vis';
import cvs from 'csvtojson';
import csvData from '../data/average-passengers-per-hour';

const style = {
    height: 400,
    margin: 20,
    padding: 15,
    display: 'inline-block',
};

export class AveragePassengersPerHour extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [
            ]
        }

        this.saveHintValue = this.saveHintValue.bind(this);
        this.removeHintValue = this.removeHintValue.bind(this);
    }
    componentDidMount() {
        cvs({
            noheader: true,
            headers: ["x", "y", "total_passegers"],
            colParser: {
                "x": "string",
                "y": "number",
                "total_passegers": "omit",
            }
        })
            .fromString(csvData)
            .on('json', (jsonObjct) => {
                this.setState({
                    data: [...this.state.data, jsonObjct]
                })
            })
    }

    saveHintValue(value) {
        this.setState({ value });
    }

    removeHintValue() {
        this.setState({ value: null });
    }

    render() {
        const { value } = this.state;
        return (<Paper style={style} zDepth={1}>
            <h3><strong>Average Number of Passengers per each Hour</strong></h3>
            <XYPlot
                height={300}
                width={600}
                xType="ordinal"
                yType="linear">
                <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis />
                <YAxis />
                <VerticalBarSeries
                    data={this.state.data}
                    onValueMouseOver={this.saveHintValue}
                    onValueMouseOut={this.removeHintValue}
                    color='#AB47BC'
                />
                {value ?
                    <Hint value={value}>
                        <div style={{
                            background: '#37474F',
                            padding: 10,
                        }}>
                            {value.y} passengers at {value.x} Hour
                        </div>
                    </Hint>
                    :
                    null
                }
            </XYPlot>
        </Paper>)
    }
}

export default AveragePassengersPerHour;


