import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import { grey700, grey800 } from 'material-ui/styles/colors';
import AppBar from 'material-ui/AppBar';
import TotalMoneyEarned from './analysis-components/total-money-earned';
import NumberOfRidesPerDay from './analysis-components/number-of-rides-per-day';
import AveragePassengersPerHour from './analysis-components/average-passengers-per-hour';
import AverageTravelTimePerHour from './analysis-components/average-travel-time-per-hour';
import TopTenPickUpCells from './analysis-components/top-ten-pick-up-cells';
import TopTenDropOffCells from './analysis-components/top-ten-drop-off-cells';
import PaymentPercentages from './analysis-components/payment-type-percentages'
import TopTenPickUpDropOffCells from './analysis-components/top-ten-pick-up-and-drop-off-cells';
import PricePerDistanceStats from './analysis-components/price-per-distance-stats';
import PricePerDistanceTable from './analysis-components/price-per-distance-table';
import DirectVsDivertedPie from './analysis-components/direct-vs-diverted-pie';
import DirectVsDiverted from './analysis-components/direct-vs-diverted';

import './App.css';
import '../node_modules/react-vis/dist/style.css';

import logo from './logo.png';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: grey800,
    primary2Color: grey800,
    primary3Color: grey700,
  },
});

class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <AppBar title="New York Yellow Taxi Data Analysis" iconElementLeft={
          <img src={logo} className="App-logo" alt="logo" />
        }
        />
        <div className='widget-container'>
          {TotalMoneyEarned}
          <NumberOfRidesPerDay />
          <AveragePassengersPerHour />
          <AverageTravelTimePerHour />
          <PaymentPercentages />
          <TopTenPickUpCells />
          <TopTenDropOffCells />
          <TopTenPickUpDropOffCells />
          <PricePerDistanceStats />
          <PricePerDistanceTable />
          <DirectVsDivertedPie />
          <DirectVsDiverted />
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
