package iit.ac.hemikak.tasks;

import iit.ac.hemikak.tasks.utils.CountAverageTuple;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;

/**
 * https://github.com/geftimov/hadoop-map-reduce-patterns/blob/master/src/main/java/com/eftimoff/mapreduce/summarization/numerical/average/Average.java
 */
public class AvgNoPassengersPerHourAllDays {
    public static class AverageMapper extends Mapper<LongWritable, Text, IntWritable, CountAverageTuple> {
        private IntWritable outHour = new IntWritable();
        private CountAverageTuple countAverageTuple = new CountAverageTuple();
    
        public static final int PICKUP_DATE_INDEX = 1;
        public static final int PASSENGER_COUNT_INDEX = 3;
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            if (key.get() != 0) {
                String pickupDateTime = value.toString().split(",")[PICKUP_DATE_INDEX];
                String pickupTime = pickupDateTime.split(" ")[1];
                int pickupHour = Integer.parseInt(pickupTime.split(":")[0]);
        
                int passengerCount = Integer.parseInt(value.toString().split(",")[PASSENGER_COUNT_INDEX]);
        
                this.outHour.set(pickupHour);
                this.countAverageTuple.setRideCount(1);
                this.countAverageTuple.setAveragePassengerCount(passengerCount);
                context.write(outHour, this.countAverageTuple);
            }
        }
    }
    
    public static class AverageReducer extends Reducer<IntWritable, CountAverageTuple, IntWritable, CountAverageTuple> {
        private CountAverageTuple result = new CountAverageTuple();
        
        @Override
        public void reduce(IntWritable key, Iterable<CountAverageTuple> values, Context context)
                throws IOException, InterruptedException {
            float sum = 0;
            float count = 0;
            // Iterate through all input values for this key
            for (CountAverageTuple val : values) {
                sum += val.getRideCount() * val.getAveragePassengerCount();
                count += val.getRideCount();
            }
    
            result.setRideCount(count);
            result.setAveragePassengerCount(sum / count);
            context.write(key, result);
        }
    }
    
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args)
                .getRemainingArgs();
    
        if (otherArgs.length != 2) {
            System.err.println("Usage: AvgNoPassengersPerHourAllDays <taxi-data> <out>");
            System.exit(2);
        }
    
        Path input = new Path(otherArgs[0]);
        Path outputDir = new Path(otherArgs[1]);
    
        Job job = Job.getInstance(conf, "Average Number Passengers Per Hour All Days");
        job.setJarByClass(AvgNoPassengersPerHourAllDays.class);
        job.setMapperClass(AvgNoPassengersPerHourAllDays.AverageMapper.class);
        job.setReducerClass(AvgNoPassengersPerHourAllDays.AverageReducer.class);
        job.setCombinerClass(AvgNoPassengersPerHourAllDays.AverageReducer.class);
        
        job.setOutputKeyClass(IntWritable.class);
        job.setOutputValueClass(CountAverageTuple.class);
    
        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, outputDir);
    
        int code = job.waitForCompletion(true) ? 0 : 1;
        
        // Clean up empty output directory
//        FileSystem.get(conf).delete(outputDir, true);
    
        System.exit(code);
    }
}
