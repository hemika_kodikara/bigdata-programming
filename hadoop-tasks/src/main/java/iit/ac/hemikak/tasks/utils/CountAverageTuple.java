package iit.ac.hemikak.tasks.utils;

import org.apache.hadoop.io.Writable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 *
 */
public class CountAverageTuple implements Writable {
    private float averagePassengerCount = 0;
    private float rideCount;
    
    public void readFields(DataInput in) throws IOException {
        averagePassengerCount = in.readFloat();
        rideCount = in.readFloat();
    }
    
    public void write(DataOutput out) throws IOException {
        out.writeFloat(averagePassengerCount);
        out.writeFloat(rideCount);
    }
    
    public float getAveragePassengerCount() {
        return averagePassengerCount;
    }
    
    public void setAveragePassengerCount(float passengerCount) {
        this.averagePassengerCount = passengerCount;
    }
    
    public float getRideCount() {
        return rideCount;
    }
    
    public void setRideCount(float rideCount) {
        this.rideCount = rideCount;
    }
    
    @Override
    public String toString() {
        return averagePassengerCount + "\t" + rideCount;
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Float.floatToIntBits(rideCount);
        result = prime * result + Float.floatToIntBits(averagePassengerCount);
        return result;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        CountAverageTuple other = (CountAverageTuple) obj;
        if (Float.floatToIntBits(rideCount) != Float.floatToIntBits(other.rideCount))
            return false;
        if (Float.floatToIntBits(averagePassengerCount) != Float.floatToIntBits(other.averagePassengerCount))
            return false;
        return true;
    }
    
}
