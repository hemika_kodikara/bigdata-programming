package iit.ac.hemikak.tasks;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.CounterGroup;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;

/**
 * Calculates the total money earned from all trips.
 */
public class RidesOnEachDay {
    public static class RidesOnEachDayMapper extends Mapper<LongWritable, Text, NullWritable, NullWritable> {
        
        public static final int PICKUP_DATE_INDEX = 1;
        public static final int DROPOFF_DATE_INDEX = 2;
        public static final String RIDES_COUNTER_GROUP = "RidesPerDayGroup";
        
        @Override
        public void map(LongWritable key, Text value, Context context) {
            if (key.get() != 0) {
                
                String pickUpDateTime = value.toString().split(",")[PICKUP_DATE_INDEX];
                String pickUpDate = pickUpDateTime.split(" ")[0];
    
                context.getCounter(RIDES_COUNTER_GROUP, pickUpDate).increment(1);
            }
        }
    }
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args)
                .getRemainingArgs();
    
        if (otherArgs.length != 2) {
            System.err.println("Usage: RidesOnEachDay <taxi-data> <out>");
            System.exit(2);
        }
    
        Path input = new Path(otherArgs[0]);
        Path outputDir = new Path(otherArgs[1]);
    
        Job job = Job.getInstance(conf, "Rides On Each Day");
        job.setJarByClass(RidesOnEachDay.class);
    
        job.setMapperClass(RidesOnEachDayMapper.class);
        job.setNumReduceTasks(0);
    
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(NullWritable.class);
    
        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, outputDir);
    
        int code = job.waitForCompletion(true) ? 0 : 1;
    
        if (code == 0) {
            for (Counter counter : job.getCounters().getGroup(RidesOnEachDayMapper.RIDES_COUNTER_GROUP)) {
                System.out.println(counter.getDisplayName() + "," + counter.getValue());
            }
        }
    
        // Clean up empty output directory
        FileSystem.get(conf).delete(outputDir, true);
    
        System.exit(code);
    }
}
