package iit.ac.hemikak.tasks;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

import java.io.IOException;

/**
 * Calculates the total money earned from all trips.
 */
public class TotalMoneyEarned {
    public static class TotalMoneyEarnedMapper extends Mapper<LongWritable, Text, NullWritable, NullWritable> {
        
        public static final int TOTAL_AMOUNT_INDEX = 18;
        public static final String TOTAL_EARNED_COUNTER_GROUP = "Total_Earned";
        
        @Override
        public void map(LongWritable key, Text value, Context context) {
            if (key.get() != 0) {
                String totalAmountValueInRow = value.toString().split(",")[TOTAL_AMOUNT_INDEX];
                if (totalAmountValueInRow != null && !totalAmountValueInRow.isEmpty()) {
                    double safeDouble = Double.parseDouble(totalAmountValueInRow) * 100;
                    context.getCounter(TOTAL_EARNED_COUNTER_GROUP, "Total").increment((long)safeDouble);
                }
            }
        }
    }
    
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args)
                .getRemainingArgs();
    
        if (otherArgs.length != 2) {
            System.err.println("Usage: TotalMoneyEarned <taxi-data> <out>");
            System.exit(2);
        }
    
        Path input = new Path(otherArgs[0]);
        Path outputDir = new Path(otherArgs[1]);
    
        Job job = Job.getInstance(conf, "Total Money Earned");
        job.setJarByClass(TotalMoneyEarned.class);
    
        job.setMapperClass(TotalMoneyEarnedMapper.class);
        job.setNumReduceTasks(0);
    
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(NullWritable.class);
    
        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, outputDir);
    
        int code = job.waitForCompletion(true) ? 0 : 1;
    
        if (code == 0) {
            for (Counter counter : job.getCounters().getGroup(TotalMoneyEarnedMapper.TOTAL_EARNED_COUNTER_GROUP)) {
                double valueInDouble = counter.getValue();
                System.out.println(counter.getDisplayName() + "\t" + (valueInDouble / 100));
            }
        }
    
        // Clean up empty output directory
        FileSystem.get(conf).delete(outputDir, true);
    
        System.exit(code);
    }
}
